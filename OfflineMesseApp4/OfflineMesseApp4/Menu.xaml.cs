﻿using Grünenthal.HubspotService;
using Grünenthal.Model;
using Plugin.Connectivity;
using Plugin.Toast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Grünenthal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : ContentPage
    {
        public int AsyncContacts { get; set; }
        public int ContactsToday { get; set; }
        private IPageService _pageService;
        public Menu(IPageService pageService)
        {
            this.Appearing += CP_Appearing;
            AsyncContacts = App.Database.CountAllAsyncContacts().Result;
            ContactsToday = App.Database.CountAllContactsOfTodayAsync().Result;
            InitializeComponent();
            BindingContext = this;
            _pageService = pageService;
            Device.StartTimer(new TimeSpan(0, 0, 5), HandleConnectionPeriodically);
                               }

        async void Button_ClickedAsync(object sender, EventArgs e)
        {
            await _pageService.PushAsync(new MainPage(new PageService()));
        }


        async void TapCommand(object sender, EventArgs e)
        {
            await _pageService.PushAsync(new ListContacts());
        }

        async void OpenContactListToday(object sender, EventArgs e)
        {
            await _pageService.PushAsync(new ListContactsToday());
        }


        private void CP_Appearing(object sender, EventArgs e)
        {
            asyncContacts.Text = App.Database.CountAllAsyncContacts().Result.ToString();
            contactsToday.Text = App.Database.CountAllContactsOfTodayAsync().Result.ToString();
            // deleteOldContacts();
            HandleConnectivityCheck();
        }

        private bool HandleConnectionPeriodically()
        {
            HandleConnectivityCheck();
            return true;
        }

        public void CheckConnectivity(object sender, EventArgs e)
        {
            HandleConnectivityCheck();
            CrossToastPopUp.Current.ShowToastMessage("Status wurde aktualisiert!");
        }

        private void HandleConnectivityCheck()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                contactsToday.Text = App.Database.CountAllContactsOfTodayAsync().Result.ToString();
                asyncContacts.Text = App.Database.CountAllAsyncContacts().Result.ToString();
                labelStatus.Text = "Online";
                var contactsToSyncronize = App.Database.GetUnsynchronizedContacts().Result;
                if (contactsToSyncronize.Count > 0)
                {
                    labelStatus.Text = "Kontakte werden hochgeladen...";
                    ContactService contactService = new ContactService();
                    foreach (Contact contact in contactsToSyncronize)
                    {
                        if (!contact.Error)
                        {
                            if (contactService.CreateContact(contact))
                            {
                                contact.Synchronized = true;
                                contact.Error = false;
                                App.Database.SaveContactAsync(contact);
                            }
                            else
                            {
                                contact.Error = true;
                            }
                        }
                    }

                    labelStatus.Text = "Online";
                }
            }
            else
            {
                labelStatus.Text = "Offline";
            }
        }
    }
}