﻿using Grünenthal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Toast;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Grünenthal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactDetail : ContentPage
    {
        //public bool DeleteBtn { get; set; }
        public Contact Contact { get; set; }
        private PageService _pageService;
        public ContactDetail(Contact contact, PageService pageService/*,bool deleteBtn = true*/)
        {
            InitializeComponent();
            this._pageService = pageService;
            Contact = contact;
            firstname.Text = contact.FirstName;
            lastname.Text = contact.LastName;
            address.Text = contact.StreetAndHouseNo == null ? "Keine Angabe" : contact.StreetAndHouseNo;
            zipcode.Text = contact.ZipCode;
            city.Text = contact.City;
            mail.Text = contact.Mail;
            subjectArea.Text = contact.SubjectArea;
            country.Text = contact.Country;
            title.Text = contact.Title;
           
            deleteBtn.IsVisible = !contact.Synchronized;
        }

        async void DeleteContact(object sender, EventArgs e)
        {
            await App.Database.DeleteContact(Contact);
            await _pageService.PopAsync();
            CrossToastPopUp.Current.ShowToastMessage("Kontakt wurde erfolgreich entfernt");
        }
    }
}