﻿using Grünenthal.Data;
using Grünenthal.Model;
using System;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Grünenthal
{
    public partial class App : Application
    {
        // public const string HAPI_KEY = "cc6e46de-564e-4186-a91d-41f71858cf51";
        public const string HAPI_KEY = "dfade646-515b-4eaf-9018-378e02288e0b";
        public const string URL = "https://api.hubapi.com/";
        public const string URL_CONTACT = "https://api.hubapi.com/contacts/v1/contact/";


        static ContactDatabase database;
        public static ContactDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new ContactDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "ContactSQLite.db3"));
                }
                return database;
            }
        }
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Menu(new PageService()));

            //Contact c = new Contact() { Name = "TestName", LastName = "LastName" };
            List<Contact> list = Database.GetContactsAsynch().Result;
        }


        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes

        }

        private bool connectionCheck()
        {
            return true;
        }
    }
}