﻿using Grünenthal.Model;
using Grünenthal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Grünenthal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListContactsToday : ContentPage
    {
        public ListContactsTodayViewModel ListContactsTodayViewModel { get; set; }

        public ListContactsToday()
        {
            ListContactsTodayViewModel = new ListContactsTodayViewModel(new PageService());

            InitializeComponent();
            BindingContext = ListContactsTodayViewModel;
            this.Appearing += CP_Appearing;
            //var contacts = App.Database.GetContactsOfTodayAsync().Result;
            //contactList.ItemsSource = contacts;
        }
        private void CP_Appearing(object sender, EventArgs e)
        {
            var rst = App.Database.GetContactsOfTodayAsync().Result;
            //var rst = App.Database.GetContactsAsynch().Result;
            ListContactsTodayViewModel.Contacts.Clear();
            ListContactsTodayViewModel.Contacts.InsertRange(rst);
        }
    }
}