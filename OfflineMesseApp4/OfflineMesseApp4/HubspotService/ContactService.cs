﻿using Grünenthal.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Grünenthal.HubspotService
{
    public class ContactService
    {
        /// <summary>
        /// Post a contact object to 
        /// the hubspot APxI
        /// </summary>
        /// <param name="c"></param>
        /// <returns> When the response Code is 200 the return would be true</returns>
        public bool CreateContact(Contact c)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", App.HAPI_KEY);
                var j = "{\"properties\":[{\"property\":\"anrede\",\"value\":\"" + c.Salutation + "\"}, {\"property\":\"email\",\"value\":\"" + c.Mail + "\"},{\"property\":\"titel\",\"value\":\"" + c.Title + "\"},{\"property\":\"firstname\",\"value\":\"" + c.FirstName + "\"},{\"property\":\"lastname\",\"value\":\"" + c.LastName + "\"},{\"property\":\"address\",\"value\":\"" + c.StreetAndHouseNo + "\"},{\"property\":\"city\",\"value\":\"" + c.City + "\"},{\"property\":\"country\",\"value\":\"" + c.Country + "\"},{\"property\":\"postleitzahl\",\"value\":\"" + c.ZipCode + "\"},{\"property\":\"fachrichtung\",\"value\":\"" + c.SubjectArea + "\"},{\"property\":\"hs_legal_basis\",\"value\":\"Freely given consent from contact\"}]}   ";
                var content = new StringContent(j, Encoding.UTF8, "application/json");
                HttpResponseMessage responseMsg = client.PostAsync(App.URL_CONTACT + "?hapikey=" + App.HAPI_KEY, content).Result;
                if (responseMsg.IsSuccessStatusCode)
                {
                    return true;
                }
                if(responseMsg.StatusCode == System.Net.HttpStatusCode.Conflict)
                {
                    HttpResponseMessage rspGet = client.GetAsync(App.URL_CONTACT + "email/" + c.Mail + "/profile?hapikey=" + App.HAPI_KEY).Result;
                    string getContent = rspGet.Content.ReadAsStringAsync().Result;
                    var tmp = getContent.Split(':');
                    var vid = tmp[1].Split(',');

                    Console.WriteLine("vid ==> "  + vid[0]);
                    var putContent = new StringContent(j, Encoding.UTF8, "application/json");
                    HttpResponseMessage tspPut = client.PostAsync(App.URL_CONTACT + "vid/" + vid[0] + "/profile?hapikey=" + App.HAPI_KEY,putContent).Result;
                    if (tspPut.IsSuccessStatusCode)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
       
    }
}
