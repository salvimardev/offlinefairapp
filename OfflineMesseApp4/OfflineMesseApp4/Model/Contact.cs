﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Grünenthal.Model
{
    public class Contact
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        //title
        public string Title { get; set; }
        //firstname
        public string FirstName { get; set; }
        //lastname
        public string LastName { get; set; }
        //salutation
        public string Salutation { get; set; }
        //address
        public string StreetAndHouseNo { get; set; }
        //zip
        public string ZipCode { get; set; }
        //city
        public string City { get; set; }
        //email
        public string Mail { get; set; }
        //country
        public string Country  { get; set; }
        //subjectArea
        public string SubjectArea { get; set; }



        public int CreatedAt { get; set; }
        public bool Error { get; set; }
        public string CreatedAtFormatted { get { return DateTimeOffset.FromUnixTimeSeconds(CreatedAt).DateTime.ToString("dd.MM.yyyy  H:mm"); } }
        public bool NewsletterOptIn { get; set; }
        public string NewsletterOptInString { get; set; }
        public bool MarketingOptIn { get; set; }
        public string MarketingOptInString { get; set; }
        public bool Synchronized { get; set; }
    }
}
