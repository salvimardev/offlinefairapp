﻿using Grünenthal.Model;
using Grünenthal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Grünenthal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListContacts : ContentPage
    {
        public ListContactViewModel ListContactViewModel { get; set; }

        public ListContacts()
        {
            ListContactViewModel = new ListContactViewModel(new PageService());
            InitializeComponent();
            BindingContext = ListContactViewModel;
            this.Appearing += CP_Appearing;

        }
        private void CP_Appearing(object sender, EventArgs e)
        {
            var rst = App.Database.GetUnsynchronizedContacts().Result;
            ListContactViewModel.Contacts.Clear();
            ListContactViewModel.Contacts.InsertRange(rst);
        }
    }
}