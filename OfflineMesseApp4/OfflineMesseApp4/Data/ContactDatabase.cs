﻿using Grünenthal.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grünenthal.Data
{
    public class ContactDatabase
    {
        readonly SQLiteAsyncConnection database;
        public ContactDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Contact>().Wait();
        }

        public Task<List<Contact>> GetUnsynchronizedContacts()
        {
            return database.Table<Contact>().Where(c => c.Synchronized == false).ToListAsync();
        }

        public Task<List<Contact>> GetContactsAsynch()
        {
            return database.Table<Contact>().ToListAsync();
        }

        public Task<List<Contact>> GetContactsOfTodayAsync()
        {

            int elapsedSecondsTillToday = (int)new DateTimeOffset(DateTime.Now.Date).ToUnixTimeSeconds();
            return database.Table<Contact>().Where(c => c.CreatedAt >= elapsedSecondsTillToday).ToListAsync();
        }

        public Task<int> CountAllContactsOfTodayAsync()
        {
            int elapsedSecondsTillToday = (int)new DateTimeOffset(DateTime.Now.Date).ToUnixTimeSeconds();
            return database.Table<Contact>().Where(c => c.CreatedAt >= elapsedSecondsTillToday).CountAsync();
        }

        public Task<int> CountAllAsyncContacts()
        {
            return database.Table<Contact>().Where(s => s.Synchronized == false).CountAsync();
        }

        //public Task<int> DeleteOldSynchronContacts()
        //{
        //    int elapsedSecondsTillToday = (int)new DateTimeOffset(DateTime.Now.Date).ToUnixTimeSeconds();
        //    return database.Table<Contact>().DeleteAsync(c => (c.Synchronized) || (c.CreatedAt < elapsedSecondsTillToday));
        //}
        public Task<int> DeleteContact(Contact contact)
        {
            return database.Table<Contact>().DeleteAsync(c => c.ID == contact.ID);
        }

        public Task<Contact> GetContactAsync(int id)
        {
            return database.Table<Contact>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }
        public Task<int> SaveContactAsync(Contact contact)
        {
            if (contact.ID != 0)
            {
                return database.UpdateAsync(contact);
            }
            else
            {
                return database.InsertAsync(contact);
            }
        }
    }
}
