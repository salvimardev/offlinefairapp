﻿using Grünenthal.Model;
using Grünenthal.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Grünenthal.ViewModel
{
    public class ListContactsTodayViewModel
    {
        public RangeEnabledObservableCollection<Contact> Contacts { get; set; }
        public ICommand OnSelectContact { get; set; }
        private PageService _pageService { get; set; }
        public ListContactsTodayViewModel(PageService pageService)
        {
            _pageService = pageService;
            Contacts = new RangeEnabledObservableCollection<Contact>();
            Contacts.InsertRange(App.Database.GetContactsOfTodayAsync().Result);
            OnSelectContact = new Command<Contact>(LoadContact);
        }

        async void LoadContact(Contact contact)
        {
            await _pageService.PushAsync(new ContactDetail(contact, _pageService));
        }
    }
}
