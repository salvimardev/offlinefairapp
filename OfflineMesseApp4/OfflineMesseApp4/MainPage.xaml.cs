﻿using Grünenthal.HubspotService;
using Grünenthal.Model;
using Plugin.Connectivity;
using Plugin.Toast;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Grünenthal
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    public partial class MainPage : ContentPage
    {
        private PageService _pageService;
        ContactService contactService = new ContactService();
        private bool isSendBtnEnabled = true;

        public bool IsSendBtnEnabled
        {
            get { return isSendBtnEnabled; }
            set { isSendBtnEnabled = value; }
        }


        public MainPage(PageService pageService)
        {
            _pageService = pageService;
            InitializeComponent();
            BindingContext = this;

            List<string> sex = new List<string>();
            sex.Add("Herr");
            sex.Add("Frau");
            sex.Add("Divers");
                       
            pickerSex.ItemsSource = sex;
        }
        ///*p*/rivate string tmp = "";
        //public void CheckConnectivity(object sender, EventArgs e)
        //{
        //    if (CrossConnectivity.Current.IsConnected)
        //    {

        //        //status.Text = tmp; TODO
        //        tmp += "Online";
        //    }
        //    else
        //    {
        //        //status.Text = "Offline"; TODO
        //    }
        //}

        private bool ValidateInput()
        {
            bool rst = true;
            if (pickerSex.SelectedItem == null)
            {
                rst = false;

                pickerSexValid.IsVisible = true;
            }
            else
            {
                pickerSexValid.IsVisible = false;
            }
            if(string.IsNullOrWhiteSpace(title.Text))
            {
                rst = false;
                titleValid.IsVisible = true;
            }
            if (string.IsNullOrWhiteSpace(firstName.Text))
            {
                rst = false;
                firstNameValid.IsVisible = true;
            }
            else
            {
                firstNameValid.IsVisible = false;
            }
            if (string.IsNullOrWhiteSpace(lastName.Text))
            {
                rst = false;
                lastNameValid.IsVisible = true;
            }
            else
            {
                lastNameValid.IsVisible = false;
            }
            if (string.IsNullOrWhiteSpace(zipCode.Text))
            {
                rst = false;
                zipCodeValid.IsVisible = true;
            }
            else
            {
                zipCodeValid.IsVisible = false;
            }
            if (string.IsNullOrWhiteSpace(city.Text))
            {
                rst = false;
                cityValid.IsVisible = true;
            }
            else
            {
                cityValid.IsVisible = false;
            }
            if (!IsValidEmail(mail.Text))
            {
                rst = false;
                mailValid.IsVisible = true;
            }
            else
            {
                mailValid.IsVisible = false;
            }
            if(string.IsNullOrWhiteSpace(country.Text))
            {
                rst = false;
                countryValid.IsVisible = true;
            }
            else
            {
                countryValid.IsVisible = false;
            }
            if(string.IsNullOrWhiteSpace(subjectArea.Text))
            {
                rst = false;
                subjectAreaValid.IsVisible = true;
            }
            else
            {
                subjectAreaValid.IsVisible = false;
            }
            if(!privacyCheck.IsToggled)
            {
                rst = false;
                privacyCheckValid.IsVisible = true;
            }
            else
            {
                privacyCheckValid.IsVisible = false;
            }
            return rst;
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }
            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }
            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public void OpenPrivacy(Object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.grunenthal.com/en/footer-links/disclaimer")); // TODO find offline version
        }

        async void SendRequest(object sender, EventArgs e)
        {
            if (!ValidateInput())
            {
                return;
            }
            if (!IsSendBtnEnabled)
            {
                return;
            }
            if(!privacyCheck.IsToggled)
            {
                return;
            }
            IsSendBtnEnabled = false;
            Console.WriteLine("Send Request!!!");
            Contact c = new Contact()
            {
                //  Salutation = pickerSex.SelectedItem.ToString(),
                Title = title.Text,
                FirstName = firstName.Text,
                LastName = lastName.Text,
                StreetAndHouseNo = streetAndHouseNo.Text,
                ZipCode = zipCode.Text,
                City = city.Text,
                Country = country.Text,
                Mail = mail.Text,
                SubjectArea = subjectArea.Text,
                NewsletterOptIn = privacyCheck.IsEnabled,
                MarketingOptIn = marketingCheck.IsEnabled,

                CreatedAt = (int)DateTimeOffset.Now.ToUnixTimeSeconds()
            };
            
            if (c.NewsletterOptIn)
            {
                c.NewsletterOptInString = "ja";
            }
            else
            {
                c.NewsletterOptInString = "nein";
            }
            if(c.MarketingOptIn)
            {
                c.MarketingOptInString = "ja";
            }
            else
            {
                c.MarketingOptInString = "nein";
            }
            if (pickerSex.SelectedItem != null)
            {
                switch (pickerSex.SelectedIndex)
                {
                    case 0: c.Salutation = "Herr"; break;
                    case 1: c.Salutation = "Frau"; break;
                    case 2: c.Salutation = "Divers"; break;
                }
            }

            //Checking internet Connection
            if (CrossConnectivity.Current.IsConnected)
            {
                if (contactService.CreateContact(c))
                {
                    c.Synchronized = true;
                    c.Error = false;
                }
                else
                {
                    c.Error = true;
                }

                await App.Database.SaveContactAsync(c);
            }
            else
            {
                await App.Database.SaveContactAsync(c);
            }
            await _pageService.PopAsync();
            CrossToastPopUp.Current.ShowToastMessage("Kontakt wurde erfolgreich erstellt");
            IsSendBtnEnabled = true;
        }
    }
}

